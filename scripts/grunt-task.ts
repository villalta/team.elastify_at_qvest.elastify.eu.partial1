import * as cp from 'child_process'
import * as fs from 'fs'
import { js2xml, xml2js } from 'xml-js'; 

const retrieveClasses = () => {
    let out = cp.execSync("sfdx force:source:retrieve -x manifest/package.xml --json").toString();
    
    console.log('Retrieved all classes from chosen org');
    
    const ignoreFiles = [
        'ToolingAPI',
        'ToolingAPITest',
        'MetadataService',
        'MetadataServiceTest',
    ]
    
    cp.execSync(`rm ${ignoreFiles.map(file => 'force-app/main/default/classes/'+file+'.cls').join(' ')} ${ignoreFiles.map(file => 'force-app/main/default/classes/'+file+'.cls-meta.xml').join(' ')}`)
    
    console.log('Deleted metadata found in ignoredFiles');
    
    let file: any = xml2js(fs.readFileSync('manifest/package.xml').toString(), { compact: true });
    
    let newMembers = [];
    
    for(let _file of JSON.parse(out).result.inboundFiles) {
        if( !_file.filePath.includes('-meta.xml') ) {
            if( ignoreFiles.indexOf(_file.fullName) < 0 ) {
                newMembers.push({ _text: _file.fullName });
                // console.log('Retrieved', inboundFile.fullName);
                // console.log('Retrieved', _file);
            }
        }
    }    
    
    file.Package.types.members = newMembers
    
    fs.writeFileSync('manifest/package.xml', js2xml(file, { spaces: '\t', compact: true }));
    
    console.log('Overwrite package.xml with the relevant metadata to be upgraded');
}

const commitChanges = () => {
    cp.execSync('git add --all');

    cp.execSync('git commit -m "chore: retrieved all apex classes with their initial versions"');

    console.log('Committed the initial versions');
}

const upgradeVersion = () => {
    const CLASSES_PATH = 'force-app/main/default/classes';

    let classesDir = fs.readdirSync(CLASSES_PATH);

    let  classesMeta = classesDir.filter(c => c.includes('cls-meta.xml'));

    for(let classMeta of classesMeta) {
        let file = xml2js(fs.readFileSync(CLASSES_PATH+'/'+classMeta).toString()); 

        file.elements[0].elements.filter(el => el.name == 'apiVersion')[0].elements[0].text = '52.0';

        // console.log(file.elements[0].elements.filter(el => el.name == 'apiVersion')[0].elements[0] );

        fs.writeFileSync(CLASSES_PATH+'/'+classMeta, js2xml(file, { spaces: '\t' }));

        // console.log('Changed', classMeta);
    }

    console.log('Upgraded the version of the relevant metadata');
}

retrieveClasses(); 

commitChanges(); 

upgradeVersion();